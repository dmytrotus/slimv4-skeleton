<?php

namespace NN\Services;

class FirstService
{
    public function getText(): string
    {
        return 'Hello from First Service';
    }
}
