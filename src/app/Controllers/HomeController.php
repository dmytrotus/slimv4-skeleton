<?php

namespace NN\Controllers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use NN\Services\FirstService;

class HomeController
{
    public function __construct(
        private FirstService $service
    ) {
    }

    public function home(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $text = $this->service->getText();

        $response->getBody()->write($text);

        return $response;
    }
}
